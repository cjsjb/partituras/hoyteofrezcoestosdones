\context Staff = "soprano" \with { \consists Ambitus_engraver} <<
	\set Staff.instrumentName = "Soprano"
	\set Staff.shortInstrumentName = "S."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-soprano" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\clef "treble"
		\key c \major

		R1*8  |
		c' 4. g' 8 ~ g' 2 ~  |
%% 10
		g' 2 r4 c' 8 c'  |
		d' 8 d' e' d' 4 d' 4. ~  |
		d' 2 r4 c' 8 c'  |
		c' 4. g' 8 ~ g' 2 ~  |
		g' 2 r4 c' 8 c'  |
%% 15
		d' 8 d' e' d' 4 d' 4. ~  |
		d' 1  |
		r4 f' 8 f' f' 4. f' 8  |
		g' 8 a' 4 g' g' 8 c' c'  |
		g' 4 g' g' 8 f' 4 e' 8 ~  |
%% 20
		e' 4. r8 c' e' 4 f' 8 ~  |
		f' 4 e' d' 8 c' 4 f' 8 ~  |
		f' 4 e' d' 8 c' 4 d' 8 ~  |
		d' 1 ~  |
		d' 2. r4  |
%% 25
		c' 4. g' 8 ~ g' 2 ~  |
		g' 2 r4 c' 8 c'  |
		d' 8 d' e' d' 4 d' 4. ~  |
		d' 2 r4 c' 8 c'  |
		c' 4. g' 8 ~ g' 2 ~  |
%% 30
		g' 2 r4 c' 8 c'  |
		d' 8 d' e' d' 4 d' 4. ~  |
		d' 1  |
		r4 f' 8 f' f' 4. f' 8  |
		g' 8 a' 4 g' g' 8 c' 4  |
%% 35
		g' 2 g' 8 f' 4 e' 8 ~  |
		e' 4. c' 8 c' e' 4 f' 8 ~  |
		f' 4 e' d' 8 c' 4 f' 8 ~  |
		f' 4 e' 4. r8 c' c'  |
		g' 8 g' a' g' ~ g' 2 ~  |
%% 40
		g' 4. r8 c' e' 4 g' 8 ~  |
		g' 1  |
		r4 g' a' 8 g' 4 g' 8 ~  |
		g' 1  |
		r4 g' g' g'  |
%% 45
		a' 4 g' f' 8 e' 4 a' 8 ~  |
		a' 4 g' 4. r8 e' c'  |
		d' 1  |
		r2 c' 8 e' 4 g' 8 ~  |
		g' 1  |
%% 50
		r4 g' 8 g' a' g' 4 g' 8 ~  |
		g' 1  |
		r4 g' g' g'  |
		a' 2 g' 8 f' 4 e' 8 ~  |
		e' 8 d' 2 r8 c' d'  |
%% 55
		e' 4. f' 8 e' d' 4 c' 8 ~  |
		c' 2 r4 d' 8 e'  |
		f' 4 e' d' 8 c' 4 d' 8 ~  |
		d' 4 d' e' 8 e' 4 c' 8 ~  |
		c' 1  |
%% 60
		R1  |
		r4 r8 f' f' f' f' g' ~  |
		g' 8 g' a' g' 4 g' 8 c' c'  |
		g' 4 g' g' 8 f' 4 e' 8 ~  |
		e' 4 r8 c' c' d' e' f' ~  |
%% 65
		f' 4 e' r d' 8 c'  |
		f' 4 e' 8 e' d' c' 4 d' 8 ~  |
		d' 1 ~  |
		d' 1  |
		r4 r8 f' f' f' f' g' ~  |
%% 70
		g' 8 a' 4 g' r8 c' c'  |
		g' 4 g' 8 g' g' f' f' e' ~  |
		e' 4 r8 c' c' e' 4 f' 8 ~  |
		f' 4 e' 8 e' d' c' 4 f' 8 ~  |
		f' 4 e' d' 8 c' 4 g' 8 ~  |
%% 75
		g' 1  |
		r2 c' 8 e' 4 g' 8 ~  |
		g' 1  |
		r4 g' a' 8 g' 4 g' 8 ~  |
		g' 1  |
%% 80
		r4 g' g' g'  |
		a' 4 g' f' 8 e' 4 a' 8 ~  |
		a' 4 g' 4. r8 e' c'  |
		d' 1  |
		r2 c' 8 e' 4 g' 8 ~  |
%% 85
		g' 1  |
		r4 g' 8 g' a' g' 4 g' 8 ~  |
		g' 1  |
		r4 g' g' g'  |
		a' 2 g' 8 f' 4 e' 8 ~  |
%% 90
		e' 8 d' 2 r8 c' d'  |
		e' 4. f' 8 e' d' 4 c' 8 ~  |
		c' 2 r4 d' 8 e'  |
		f' 4 e' d' 8 c' 4 d' 8 ~  |
		d' 4 d' 2. ~  |
%% 95
		d' 4 r e' 8 e' 4 c' 8 ~  |
		c' 1  |
		R1  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-soprano" {
		Pa -- dre, __
		hoy "te o" -- frez -- "co es" -- tos do -- nes __
		y te pi -- do __
		ben -- di -- gas mis la -- bo -- res; __

		son el fru -- to de la tie -- rra,
		son mi dia -- rio ca -- mi -- nar, __
		son mi vi __ da, son "mi en" -- tre __ "ga y"
		mi ver -- dad. __

		Pa -- dre, __
		hoy "yo en" ti me con -- fí -- o __
		y "te o" -- frez -- co __
		cuan -- to he re -- ci -- bi -- do. __

		To -- do tú me lo has da -- do,
		es tu -- yo mi Dios, __
		pues cuan -- "to en" mí __ tú has con -- fia __ do
		te doy cuen -- ta, Se -- ñor. __

		Tó -- ma -- los, __
		ben -- dí -- ce -- los, __
		con -- viér -- te -- los en cuer -- "po y" san -- gre
		de Je -- sús.

		Tó -- ma -- los, __
		a -- li -- men -- ta hoy __
		al pue -- blo que tú com -- pras -- te
		con la san -- gre de Je -- sús, __
		al mo -- rir él por no -- so -- tros
		en la cruz. __

		Cual gra -- no de tri -- go que mue -- re
		pa -- ra fer -- men -- tar el pan, __
		a -- sí sea mi vi -- da,
		pa -- ra dar vi -- "da a" los de -- más. __
		Si vi -- "vo es" por ti, __ Se -- ñor
		y si mue -- ro por ti mo -- ri -- ré; __
		pues vi -- "vo o" muer -- to so -- lo de ti __
		yo quie -- ro ser. __

		Tó -- ma -- los, __
		ben -- dí -- ce -- los, __
		con -- viér -- te -- los en cuer -- "po y" san -- gre
		de Je -- sús.

		Tó -- ma -- los, __
		a -- li -- men -- ta hoy __
		al pue -- blo que tú com -- pras -- te
		con la san -- gre de Je -- sús, __
		al mo -- rir él por no -- so -- tros __
		en la cruz. __
	}
>>
