\context Staff = "tenor" \with { \consists Ambitus_engraver} <<
	\set Staff.instrumentName = "Tenor"
	\set Staff.shortInstrumentName = "T."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-tenor" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\clef "treble_8"
		\key c \major

		R1*8  |
		c 4. g 8 ~ g 2 ~  |
%% 10
		g 2 r4 e 8 e  |
		f 8 f g f 4 f 4. ~  |
		f 2 r4 e 8 e  |
		e 4. c' 8 ~ c' 2 ~  |
		c' 2 r4 e 8 e  |
%% 15
		f 8 f g f 4 f 4. ~  |
		f 1  |
		r4 a 8 a a 4. a 8  |
		b 8 c' 4 b b 8 e e  |
		c' 4 c' c' 8 b 4 a 8 ~  |
%% 20
		a 4. r8 a a 4 a 8 ~  |
		a 4 a a 8 a 4 a 8 ~  |
		a 4 a b 8 c' 4 b 8 ~  |
		b 1 ~  |
		b 2. r4  |
%% 25
		c 4. g 8 ~ g 2 ~  |
		g 2 r4 e 8 e  |
		f 8 f g f 4 f 4. ~  |
		f 2 r4 e 8 e  |
		e 4. c' 8 ~ c' 2 ~  |
%% 30
		c' 2 r4 e 8 e  |
		f 8 f g f 4 f 4. ~  |
		f 1  |
		r4 a 8 a a 4. a 8  |
		b 8 c' 4 b b 8 e 4  |
%% 35
		c' 2 c' 8 b 4 a 8 ~  |
		a 4. a 8 a a 4 a 8 ~  |
		a 4 a a 8 a 4 a 8 ~  |
		a 4 a 4. r8 a a  |
		b 8 b c' b ~ b 2 ~  |
%% 40
		b 4. r8 r2  |
		r2 c' 8 c' 4 c' 8 ( ~  |
		c' 1  |
		b 4 ) b b 8 c' 4 d' 8 ( ~  |
		d' 8 c' b 2.  |
%% 45
		c' 1 ~  |
		c' 2 ) r  |
		r4 b 8 b b c' 4 b 8 ( ~  |
		b 1  |
		c' 4. ) r8 c' c' 4 c' 8 ( ~  |
%% 50
		c' 1  |
		b 4 ) b 8 b b c' 4 d' 8 ( ~  |
		d' 8 c' b 2. )  |
		c' 2 c' 8 c' 4 b 8 ~  |
		b 8 b 4. r4 b 8 b  |
%% 55
		c' 4. d' 8 c' b 4 a 8 ~  |
		a 2 r4 a 8 b  |
		c' 4 c' c' 8 c' 4 b 8 ~  |
		b 4 b b 8 b 4 c' 8 ~  |
		c' 1  |
%% 60
		R1  |
		r4 r8 a a a a b ~  |
		b 8 b c' b 4 b 8 e e  |
		c' 4 c' c' 8 b 4 a 8 ~  |
		a 4 r8 a a a a a ~  |
%% 65
		a 4 a r a 8 a  |
		a 4 a 8 a b c' 4 b 8 ~  |
		b 1 ~  |
		b 1  |
		r4 r8 a a a a b ~  |
%% 70
		b 8 c' 4 b r8 e e  |
		c' 4 c' 8 c' c' b b a ~  |
		a 4 r8 a a a 4 a 8 ~  |
		a 4 a 8 a a a 4 a 8 ~  |
		a 4 a b 8 c' 4 b 8 ~  |
%% 75
		b 1  |
		r2 r  |
		r2 c' 8 c' 4 c' 8 ( ~  |
		c' 1  |
		b 4 ) b b 8 c' 4 d' 8 ( ~  |
%% 80
		d' 8 c' b 2.  |
		c' 1 ~  |
		c' 2 ) r  |
		r4 b 8 b b c' 4 b 8 ( ~  |
		b 1  |
%% 85
		c' 4. ) r8 c' c' 4 c' 8 ( ~  |
		c' 1  |
		b 4 ) b 8 b b c' 4 d' 8 ( ~  |
		d' 8 c' b 2. )  |
		c' 2 c' 8 c' 4 b 8 ~  |
%% 90
		b 8 b 4. r4 b 8 b  |
		c' 4. d' 8 c' b 4 a 8 ~  |
		a 2 r4 a 8 b  |
		c' 4 c' c' 8 c' 4 b 8 ~  |
		b 4 b 2. ~  |
%% 95
		b 4 r b 8 b 4 c' 8 ~  |
		c' 1  |
		R1  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-tenor" {
		Pa -- dre, __
		hoy "te o" -- frez -- "co es" -- tos do -- nes __
		y te pi -- do __
		ben -- di -- gas mis la -- bo -- res; __

		son el fru -- to de la tie -- rra,
		son mi dia -- rio ca -- mi -- nar, __
		son mi vi __ da, son "mi en" -- tre __ "ga y"
		mi ver -- dad. __

		Pa -- dre, __
		hoy "yo en" ti me con -- fí -- o __
		y "te o" -- frez -- co __
		cuan -- to he re -- ci -- bi -- do. __

		To -- do tú me lo has da -- do,
		es tu -- yo mi Dios, __
		pues cuan -- "to en" mí __ tú has con -- fia __ do
		te doy cuen -- ta, Se -- ñor. __

		Tó -- ma -- los, __
		ben -- dí -- ce -- los, __
		%con -- viér -- te -- los en cuer -- "po y" san -- gre
		de Je -- sús, Se -- ñor. __

		Tó -- ma -- los, __
		a -- li -- men -- ta hoy __
		"...que" tú com -- pras -- te
		con la san -- gre de Je -- sús, __
		al mo -- rir él por no -- so -- tros
		en la cruz. __

		Cual gra -- no de tri -- go que mue -- re
		pa -- ra fer -- men -- tar el pan, __
		a -- sí sea mi vi -- da,
		pa -- ra dar vi -- "da a" los de -- más. __
		Si vi -- "vo es" por ti, __ Se -- ñor
		y si mue -- ro por ti mo -- ri -- ré; __
		pues vi -- "vo o" muer -- to so -- lo de ti __
		yo quie -- ro ser. __

		Tó -- ma -- los, __
		ben -- dí -- ce -- los, __
		%con -- viér -- te -- los en cuer -- "po y" san -- gre
		de Je -- sús, Se -- ñor. __

		Tó -- ma -- los, __
		a -- li -- men -- ta hoy __
		"...que" tú com -- pras -- te
		con la san -- gre de Je -- sús, __
		al mo -- rir él por no -- so -- tros
		en la cruz. __
	}
>>
