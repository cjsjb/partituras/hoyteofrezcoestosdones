\context ChordNames
	\chords {
		\set chordChanges = ##t
		% intro
		c1 c1 d1:m d1:m
		c1 c1 d1:m d1:m

		% padre, hoy te ofrezco estos dones...
		c1 c1 d1:m d1:m
		c1 c1 d1:m d1:m

		%
		f1 g1 c2 c2:/b a1:m
		f1 d1:m g1 g1

		% padre, hoy te ofrezco estos dones...
		c1 c1 d1:m d1:m
		c1 c1 d1:m d1:m

		% todo tu me lo has dado...
		f1 g1 c2 c2:/b a1:m
		f1 d1:m g1 g1

		% tomalos...
		c1 c1 g1 g1
		f1 f1 g1 g1
		c1 c1 g1 g1
		f1 g1 c2 c2:/b a1:m
		f1 g1 c1 c1:7

		% qual grano de trigo...
		f1 g1 c2 c2:/b a1:m
		f1 d1:m g1 g1
		f1 g1 c2 c2:/b a1:m
		f1 d1:m g1 g1		

		% tomalos...
		c1 c1 g1 g1
		f1 f1 g1 g1
		c1 c1 g1 g1
		f1 g1 c2 c2:/b a1:m
		f1 g1 g4 g4 r2
		c1 c1
	}
