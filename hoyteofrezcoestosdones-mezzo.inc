\context Staff = "mezzo" \with { \consists Ambitus_engraver} <<
	\set Staff.instrumentName = "Mezzosoprano"
	\set Staff.shortInstrumentName = "M."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-mezzo" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\clef "treble"
		\key c \major

		R1*9  |
%% 10
		g' 4. e' 8 ~ e' 2  |
		r2 r4 c' 8 c'  |
		d' 8 d' e' d' 4 ( e' 8 ) g' 4  |
		r2 r4 r8 g'  |
		g' 4. e' 8 ~ e' 2  |
%% 15
		r2 r4 c' 8 c'  |
		d' 8 d' e' d' 4 ( e' 8 ) g' 4  |
		r4 f' 8 f' f' 4. f' 8  |
		d' 8 d' 4 d' d' 8 d' d'  |
		e' 4 e' e' 8 d' 4 c' 8 ~  |
%% 20
		c' 4. r8 c' c' 4 c' 8 ~  |
		c' 4 c' c' 8 c' 4 c' 8 ~  |
		c' 4 c' c' 8 c' 4 b 8 ~  |
		b 1 ~  |
		b 2. r4  |
%% 25
		R1  |
		g' 4. e' 8 ~ e' 2  |
		r2 r4 c' 8 c'  |
		d' 8 d' e' d' 4 ( e' 8 ) g' 4  |
		r2 r4 r8 g'  |
%% 30
		g' 4. e' 8 ~ e' 2  |
		r2 r4 c' 8 c'  |
		d' 8 d' e' d' 4 ( e' 8 ) g' 4  |
		r4 f' 8 f' f' 4. f' 8  |
		d' 8 d' 4 d' d' 8 d' 4  |
%% 35
		e' 2 e' 8 d' 4 c' 8 ~  |
		c' 4. c' 8 c' c' 4 c' 8 ~  |
		c' 4 c' c' 8 c' 4 c' 8 ~  |
		c' 4 c' 4. r8 c' c'  |
		d' 8 d' e' d' ~ d' 2 ~  |
%% 40
		d' 4. r8 c' e' 4 g' 8 ( ~  |
		g' 8 f' e' 2. )  |
		r4 e' e' 8 e' 4 d' 8 ~  |
		d' 1  |
		r4 d' d' d'  |
%% 45
		f' 4 f' f' 8 f' 4 f' 8 ~  |
		f' 4 f' r2  |
		r4 d' 8 d' d' e' 4 g' 8 ~  |
		g' 4. r8 g' g' 4 g' 8 ( ~  |
		g' 8 f' e' 2. )  |
%% 50
		r4 e' 8 e' e' e' 4 d' 8 ~  |
		d' 1  |
		r4 d' d' d'  |
		f' 2 f' 8 f' 4 e' 8 ~  |
		e' 8 d' 2 r8 c' d'  |
%% 55
		e' 4. f' 8 e' d' 4 c' 8 ~  |
		c' 2 r4 c' 8 c'  |
		c' 4 c' c' 8 c' 4 d' 8 ~  |
		d' 4 d' d' 8 d' 4 c' 8 ~  |
		c' 1  |
%% 60
		R1  |
		r4 r8 f' f' f' f' d' ~  |
		d' 8 d' d' d' 4 d' 8 g g  |
		e' 4 e' e' 8 d' 4 c' 8 ~  |
		c' 4 r8 c' c' c' c' c' ~  |
%% 65
		c' 4 c' r c' 8 c'  |
		c' 4 c' 8 c' b a 4 g 8 ~  |
		g 1 ~  |
		g 1  |
		r4 r8 f' f' f' f' d' ~  |
%% 70
		d' 8 d' 4 d' r8 g g  |
		e' 4 e' 8 e' e' d' d' c' ~  |
		c' 4 r8 c' c' c' 4 c' 8 ~  |
		c' 4 c' 8 c' c' c' 4 c' 8 ~  |
		c' 4 c' c' 8 c' 4 d' 8 ~  |
%% 75
		d' 1  |
		r2 c' 8 e' 4 g' 8 ( ~  |
		g' 8 f' e' 2. )  |
		r4 e' e' 8 e' 4 d' 8 ~  |
		d' 1  |
%% 80
		r4 d' d' d'  |
		f' 4 f' f' 8 f' 4 f' 8 ~  |
		f' 4 f' r2  |
		r4 d' 8 d' d' e' 4 g' 8 ~  |
		g' 4. r8 g' g' 4 g' 8 ( ~  |
%% 85
		g' 8 f' e' 2. )  |
		r4 e' 8 e' e' e' 4 d' 8 ~  |
		d' 1  |
		r4 d' d' d'  |
		f' 2 f' 8 f' 4 e' 8 ~  |
%% 90
		e' 8 d' 2 r8 c' d'  |
		e' 4. f' 8 e' d' 4 c' 8 ~  |
		c' 2 r4 c' 8 c'  |
		c' 4 c' c' 8 c' 4 d' 8 ~  |
		d' 4 d' 2. ~  |
%% 95
		d' 4 r e' 8 e' 4 e' 8 ~  |
		e' 1  |
		R1  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-mezzo" {
		Pa -- dre,
		hoy "te o" -- frez -- "co es" -- tos do __ nes
		te pi -- do
		ben -- di -- gas mis la -- bo __ res;

		son el fru -- to de la tie -- rra,
		son mi dia -- rio ca -- mi -- nar, __
		son mi vi __ da, son "mi en" -- tre __ "ga y"
		mi ver -- dad. __

		Pa -- dre,
		hoy "yo en" ti me con -- fí __ o
		"te o" -- frez -- co
		cuan -- to he re -- ci -- bi __ do.

		To -- do tú me lo has da -- do,
		es tu -- yo mi Dios, __
		pues cuan -- "to en" mí __ tú has con -- fia __ do
		te doy cuen -- ta, Se -- ñor. __

		Tó -- ma -- los, __
		ben -- dí -- ce -- los, __
		con -- viér -- te -- los en cuer -- "po y" san -- gre
		de Je -- sús, Se -- ñor. __

		Tó -- ma -- los, __
		a -- li -- men -- ta hoy __
		al pue -- blo que tú com -- pras -- te
		con la san -- gre de Je -- sús, __
		al mo -- rir él por no -- so -- tros
		en la cruz. __

		Cual gra -- no de tri -- go que mue -- re
		pa -- ra fer -- men -- tar el pan, __
		a -- sí sea mi vi -- da,
		pa -- ra dar vi -- "da a" los de -- más. __
		Si vi -- "vo es" por ti, __ Se -- ñor
		y si mue -- ro por ti mo -- ri -- ré; __
		pues vi -- "vo o" muer -- to so -- lo de ti __
		yo quie -- ro ser. __

		Tó -- ma -- los, __
		ben -- dí -- ce -- los, __
		con -- viér -- te -- los en cuer -- "po y" san -- gre
		de Je -- sús, Se -- ñor. __

		Tó -- ma -- los, __
		a -- li -- men -- ta hoy __
		al pue -- blo que tú com -- pras -- te
		con la san -- gre de Je -- sús, __
		al mo -- rir él por no -- so -- tros
		en la cruz. __
	}
>>
